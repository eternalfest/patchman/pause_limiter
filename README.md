# pause_limiter

Patchman mod to limit the use of in-game pause

#### Usage
```haxe
import hf.Hf;
import patchman.IPatch;
import patchman.Patchman;
import pause_limiter.PauseLimiter;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    pauseLimiter: PauseLimiter, // Enable pause_limiter mod
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
```

#### PauseLimiter.json example
See PauseLimiterConfig documentation for more information.
```json
{
  "firstDuration": 1,
  "multiplier": 2,
  "resetTime": 10,
  "maximumDuration": 60,
  "pauseIcon": "stop"
}
```
