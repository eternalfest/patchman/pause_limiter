package pause_limiter;

import patchman.Assert;

class RateLimiter {
  /**
   * First pause duration.
   */
  public var firstDuration(default, null): Float;

  /**
   * Pause duration multiplier.
   */
  public var multiplier(default, null): Float;

  /**
   * Time before the pause duration is reset to firstDuration.
   */
  public var resetTime(default, null): Float;
  
  /**
   * Maximum pause duration.
   */
  public var maximumDuration(default, null): Float;

  /**
   * Duration of the next pause.
   */
  public var nextPauseDuration(default, null): Float;

  /**
   * Last pause time.
   */
  public var lastPauseTime(default, null): Float;

  /**
   * Last unpause time.
   */
  public var lastUnpauseTime(default, null): Float;

  /**
   * Time at which the player can unpause.
   */
  public var unpauseTime(default, null): Float;
  
  public function new(firstDuration: Float, multiplier: Float = 0, resetTime: Float = 0, maximumDuration: Float = 0) {
    Assert.debug(firstDuration > 0);
    Assert.debug(multiplier > 0);
    Assert.debug(resetTime >= 0);
    Assert.debug(firstDuration < maximumDuration);
    this.firstDuration = firstDuration;
    this.multiplier = multiplier;
    this.resetTime = resetTime;
    this.maximumDuration = maximumDuration;
    this.nextPauseDuration = firstDuration;
    this.lastPauseTime = 0;
    this.lastUnpauseTime = 0;
    this.unpauseTime = 0;
  }

  public function onPause(time: Float): Void {
    var pauseDuration;
    if (this.lastUnpauseTime != 0 && time - this.lastUnpauseTime < this.resetTime) {
      // Short time since last unpause, add a delay before allowing unpausing
      pauseDuration = this.nextPauseDuration;
      // Next pause will be even longer
      this.nextPauseDuration = Math.min(this.maximumDuration, pauseDuration * this.multiplier);
    } else {
      // Enough time since last pause, don't add delay
      pauseDuration = 0;
      this.nextPauseDuration = this.firstDuration;
    }
    this.lastPauseTime = time;
    this.unpauseTime = time + pauseDuration;
  }

  public function onUnpause(time: Float): Void {
    this.lastUnpauseTime = time;
  }

  public function canUnpause(time: Float): Bool {
    return time >= unpauseTime;
  }

}
