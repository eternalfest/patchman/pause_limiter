package pause_limiter;

import etwin.Error;
import etwin.Obfu;
import patchman.module.Data;

@:build(patchman.Build.di())
class PauseLimiterConfig {
  /**
   * First pause duration.
   */
  public var firstDuration(default, null): Float;

  /**
   * Pause duration multiplier.
   */
  public var multiplier(default, null): Float;

  /**
   * Time before the pause duration is reset to firstDuration.
   */
  public var resetTime(default, null): Float;
  
  /**
   * Maximum pause duration.
   */
  public var maximumDuration(default, null): Float;

  /**
   * Name of a custom pause icon to use. Will use the default "Paix intérieure" icon if null.
   */
  public var pauseIcon(default, null): String;

  public function new(firstDuration: Float, multiplier: Float, resetTime: Float, maximumDuration: Float, pauseIcon: String) {
    this.firstDuration = firstDuration;
    this.multiplier = multiplier;
    this.resetTime = resetTime;
    this.maximumDuration = maximumDuration;
    this.pauseIcon = pauseIcon;
  }

  public static var DEFAULT: PauseLimiterConfig = new PauseLimiterConfig(
  1, // firstDuration
  2, // multiplier
  10, // resetTime
  60, // maximumDuration
  null // pauseIcon
  );

  @:diFactory
  public static function fromHml(dataMod: Data): PauseLimiterConfig {
    if (!dataMod.has(Obfu.raw("PauseLimiter"))) {
      return PauseLimiterConfig.DEFAULT;
    }

    var data: Dynamic = dataMod.get(Obfu.raw("PauseLimiter"));

    var firstDuration: Null<Float> = PauseLimiterConfig.getOptInt(data, Obfu.raw("firstDuration"));
    var multiplier: Null<Float> = PauseLimiterConfig.getOptInt(data, Obfu.raw("multiplier"));
    var resetTime: Null<Float> = PauseLimiterConfig.getOptInt(data, Obfu.raw("resetTime"));
    var maximumDuration: Null<Float> = PauseLimiterConfig.getOptInt(data, Obfu.raw("maximumDuration"));
    var pauseIcon: Null<String> = PauseLimiterConfig.getOptString(data, Obfu.raw("pauseIcon"));

    if (firstDuration == null) {
      firstDuration = DEFAULT.firstDuration;
    }
    if (multiplier == null) {
      multiplier = DEFAULT.multiplier;
    }
    if (resetTime == null) {
      resetTime = DEFAULT.resetTime;
    }
    if (maximumDuration == null) {
      maximumDuration = DEFAULT.maximumDuration;
    }
    if (pauseIcon == null) {
      pauseIcon = DEFAULT.pauseIcon;
    }

    return new PauseLimiterConfig(firstDuration, multiplier, resetTime, maximumDuration, pauseIcon);
  }

  private static function getOptInt(raw: Dynamic, key: String): Null<Int> {
    if (!Reflect.hasField(raw, key)) {
      return null;
    }
    var val: Dynamic = Reflect.field(raw, key);
    if (!Std.is(val, Int)) {
      throw new Error("TypeError: Invalid PauseLimiter config field: " + key);
    }
    return val;
  }

  private static function getOptString(raw: Dynamic, key: String): Null<String> {
    if (!Reflect.hasField(raw, key)) {
      return null;
    }
    var val: Dynamic = Reflect.field(raw, key);
    if (!Std.is(val, String)) {
      throw new Error("TypeError: Invalid PauseLimiter config field: " + key);
    }
    return val;
  }
}
