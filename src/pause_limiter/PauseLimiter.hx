package pause_limiter;

import etwin.flash.MovieClip;
import hf.Hf;
import hf.mode.GameMode;
import etwin.Obfu;
import etwin.ds.WeakMap;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;

@:build(patchman.Build.di())
class PauseLimiter {
  // The rate limiter uses milliseconds as time unit
  private static var RATE_LIMITERS(default, never): WeakMap<GameMode, RateLimiter> = new WeakMap();

  private static var PAUSE_MC(default, never): WeakMap<GameMode, MovieClip> = new WeakMap();

  @:diExport
  public var patch(default, null): IPatch;

  // The config uses seconds as the time unit
  private var config(default, null): PauseLimiterConfig;

  public function new(config: PauseLimiterConfig) {
    this.config = config;

    this.patch = new PatchList([
      Ref.auto(GameMode.onPause).before(function(hf: Hf, self: GameMode) {
        this.getRateLimiter(self).onPause(hf.Timer.oldTime);
      }),
      Ref.auto(GameMode.onMap).before(function(hf: Hf, self: GameMode) {
        this.getRateLimiter(self).onPause(hf.Timer.oldTime);
      }),
      Ref.auto(GameMode.onUnpause).wrap(function(hf: Hf, self: GameMode, next) {
        var limiter = this.getRateLimiter(self);
        if (limiter.canUnpause(hf.Timer.oldTime)) {
          limiter.onUnpause(hf.Timer.oldTime);
          next(self);
        }
      }),
      Ref.auto(GameMode.main).after(function(hf: Hf, self: GameMode) {
        if (self.fl_pause || self.fl_map) {
          if (this.getRateLimiter(self).canUnpause(hf.Timer.oldTime)) {
            if (PAUSE_MC.exists(self)) {
              PAUSE_MC.get(self).removeMovieClip();
              PAUSE_MC.remove(self);
            }
          } else {
            if (!PAUSE_MC.exists(self)) {
              var pauseMc = this.createPauseMc(self);
              PAUSE_MC.set(self, pauseMc);
            }
          }
        }
      })
    ]);
  }

  private function getRateLimiter(game: GameMode): RateLimiter {
    var rateLimiter: Null<RateLimiter> = RATE_LIMITERS.get(game);
    if (rateLimiter == null) {
      // The config uses seconds but rate limiter uses milliseconds, so we have to convert
      rateLimiter = new RateLimiter(
        this.config.firstDuration * 1000,
        this.config.multiplier,
        this.config.resetTime * 1000,
        this.config.maximumDuration * 1000
      );
      RATE_LIMITERS.set(game, rateLimiter);
    }
    return rateLimiter;
  }

  private function createPauseMc(game: GameMode): MovieClip {
    if (this.config.pauseIcon != null) {
      // Use user defined movieclip
      var pauseMc: MovieClip = game.depthMan.attach(this.config.pauseIcon, game.root.Data.DP_TOP);
      pauseMc._x = 345;
      pauseMc._y = 435;
      return pauseMc;
    } else {
      // Use default "Paix intérieure" sprite
      var pauseMc: MovieClip = game.depthMan.attach("9-DjM", game.root.Data.DP_TOP);
      pauseMc.gotoAndStop("7");
      pauseMc._x = 365;
      pauseMc._y = 475;
      pauseMc._xscale = 200;
      pauseMc._yscale = 200;
      return pauseMc;
    }
  }
}
