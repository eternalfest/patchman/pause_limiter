# 0.1.0 (2022-05-07)
- [**Feature**] Initial release
  - Customizable pause rate limiter migrated from  "Le Supplice des Téméraires"
